# Geo-harmonizer spatial layers

This repository includes all production steps used to produce spatial layers 
at various spatial resolutions (from 30 m to 1 km). For more info about the 
technical specifications also refer to the https://opendatascience.eu/geoharmonizer-project website.


File naming convention
----------------------
Within the Geo-harmonizer project we consistently use a file naming convention in the format e.g.:

- `dtm_elev.lowestmode_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.1.tif`

with the following fields:

- theme: e.g. `dtm`,
- variable code: e.g. `elev.lowestmode`,
- variable estimation method: e.g. `gedi.eml`,
- variable estimation type: e.g. `m`,
- spatial support (usually horizontal block) in m e.g.: `30m`,
- reference depths (vertical dimension): e.g. `0..0cm`,
- reference period begin end: e.g. `2000..2018`,
- reference area: `eumap`,
- coordinate system: e.g. `epsg3035`,
- data set version: e.g. `v0.1`,

The variable code should be used consistently in all geocomputing. Variable estimation method 
should be directly mentioned in the metadata with a reference to literature or URL where one 
can find complete documentation. 

Note from the example above the following:

-   `-` is used to indicate a field,
-   no mathematical symbols are used e.g. "+" or "/",
-   no capitalized letters are used,
-   `..` is used to indicate range,
-   `.` is used to indicate space,

Variable estimation type can be one of the following (see also the [OpenLandMap.org projecty](https://gitlab.com/openlandmap/global-layers/)):

    a.  `m` = mean value,
    b.  `d` = median value,
    c.  `l.159` = lower 68% probability threshold (quantile),
    d.  `u.841` = upper 68% probability threshold (quantile), for one-sided probability use `uo`,
    e.  `sd.1` = 1 standard deviation (`sd` can be also used),
    f.  `md` = model deviation (in the case of ensemble predictions),
    g.  `td` = cumulative difference (usually based on time-series of values),
    h.  `c` = classes i.e. factor variable,
    i.  `p` = probability or fraction,
    j.  `sse` = Shannon Scaled Entropy index,
    k.  `q` = quality index or indicator,

For temporal reference period please use the date / time format that starts with Year:

```
> format(Sys.Date(), "%Y.%m.%d")
[1] "2020.09.27"
```

If not specified otherwise, all layers contributed in the Geo-harmonizer project are 
prepared as [Cloud Optimized GeoTIFFs](https://www.cogeo.org/) 
(see metadata `-co "CO=TRUE"`) in the case of gridded data, and/or 
GeoPackage and/or [GeoPackage](https://gdal.org/drivers/vector/gpkg.html) and/or [FlatGeobuf](https://gdal.org/drivers/vector/flatgeobuf.html) files in the case of 
vector data (see [technical specifications](https://opendatascience.eu/geo-harmonizer-implementation-plan-2020-2022#Data_storage_model)).


The land mask
-------------

The `eumap` bounding box of interest is:

```
Xmin =   900,000
Ymin =   930,010
Xmax = 6,540,000
Ymax = 5,460,010
```

This is based on the [EPSG:3035](https://epsg.io/3035) reference system with the following 
proj4 parameters:

```
+init='epsg:3035'
```

The image sizes for that bounding box at various standard resolutions are:

-   30m = 188000P x 151000L,
-   100m = 56400P x 45600L,
-   250m = 22560P x 18240L,
-   1km = 56400P x 4560L,

![eumap land mask](img/eumap_landmask_preview_750px.jpg)

*Image: Area of interest (continental EU) includes EEA, without the Caribbean and Indian Ocean islands.*


Standard tiling system
----------------------
Recommended tiling system is:

-  `adm_tiling.system_30km_c_1m_s0..0m_2020_eumap_epsg3035_v0.1.gpkg`

```
eumap_epsg3035_v0.1.gpkg", layer: "tiles_landmask_30km"
with 7042 features
It has 16 fields
```

This is the 30 by 30 km tiling system. Each tile has an unique ID (see `fid`) which 
starts from the bottom left. Other attributes of the tiles include:

```
> str(tiles@data)
'data.frame':	7042 obs. of  16 variables:
 $ xl           : num  1770000 1800000 1890000 1920000 1950000 1770000 1800000 1920000 1950000 1980000 ...
 $ yl           : num  930010 930010 930010 930010 930010 ...
 $ xu           : num  1800000 1830000 1920000 1950000 1980000 1800000 1830000 1950000 1980000 2010000 ...
 $ yu           : num  960010 960010 960010 960010 960010 ...
 $ offst_y      : num  150000 150000 150000 150000 150000 149000 149000 149000 149000 149000 ...
 $ offst_x      : num  29000 30000 33000 34000 35000 29000 30000 34000 35000 36000 ...
 $ rgn_dm_y     : num  1000 1000 1000 1000 1000 1000 1000 1000 1000 1000 ...
 $ rgn_dm_x     : num  1000 1000 1000 1000 1000 1000 1000 1000 1000 1000 ...
```

Where `xl`, `yl`, `xu` and `yu` are the lower left and upper right coordinates of the tile, 
`offst_y` and `offst_x` are the offsets in the image coordinates (starting from top left), and 
`rgn_dm_y` and `rgn_dm_x` are the tile dimensions in image coordinates.


Annual land cover product (2000–2019)
-------------

This product comprises the dominant land cover class, the uncertainties and probabilities for 33 classes compatible CLC (CORINE Land Cover). All the layers are publicly available on [Open Data Science Europe](https://maps.opendatascience.eu) and through our S3 cloud object service as [COGs](https://www.cogeo.org/) (Cloud-Optimized GeoTIFFs).

Example of how access our S3 cloud object service using ``gdalinfo`` ([complete layer list](https://gitlab.com/geoharmonizer_inea/eumap/-/blob/master/gh_raster_layers.csv)):

```bash
gdalinfo /vsicurl/http://s3.eu-central-1.wasabisys.com/eumap/lcv/lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2019_eumap_epsg3035_v0.1.tif lcv_landcover_amsterdam.tif
```

Expected output:

```
Driver: GTiff/GeoTIFF
Files: /vsicurl/http://s3.eu-central-1.wasabisys.com/eumap/lcv/lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2019_eumap_epsg3035_v0.1.tif
Size is 188000, 151000
Coordinate System is:
PROJCRS["ETRS89-extended / LAEA Europe",
    BASEGEOGCRS["ETRS89",
        DATUM["European Terrestrial Reference System 1989",
            ELLIPSOID["GRS 1980",6378137,298.257222101,
                LENGTHUNIT["metre",1]]],
        PRIMEM["Greenwich",0,
            ANGLEUNIT["degree",0.0174532925199433]],
        ID["EPSG",4258]],
    CONVERSION["Europe Equal Area 2001",
        METHOD["Lambert Azimuthal Equal Area",
            ID["EPSG",9820]],
        PARAMETER["Latitude of natural origin",52,
            ANGLEUNIT["degree",0.0174532925199433],
            ID["EPSG",8801]],
        PARAMETER["Longitude of natural origin",10,
            ANGLEUNIT["degree",0.0174532925199433],
            ID["EPSG",8802]],
        PARAMETER["False easting",4321000,
            LENGTHUNIT["metre",1],
            ID["EPSG",8806]],
        PARAMETER["False northing",3210000,
            LENGTHUNIT["metre",1],
            ID["EPSG",8807]]],
    CS[Cartesian,2],
        AXIS["northing (Y)",north,
            ORDER[1],
            LENGTHUNIT["metre",1]],
        AXIS["easting (X)",east,
            ORDER[2],
            LENGTHUNIT["metre",1]],
    USAGE[
        SCOPE["unknown"],
        AREA["Europe - LCC & LAEA"],
        BBOX[24.6,-35.58,84.17,44.83]],
    ID["EPSG",3035]]
Data axis to CRS axis mapping: 2,1
Origin = (900000.000000000000000,5460010.000000000000000)
Pixel Size = (30.000000000000000,-30.000000000000000)
Metadata:
  1=111 - Urban fabric
  10=212 - Permanently irrigated arable land
  11=213 - Rice fields
  12=221 - Vineyards
  13=222 - Fruit trees and berry plantations
  14=223 - Olive groves
  15=231 - Pastures
  16=311 - Broad-leaved forest
  17=312 - Coniferous forest
  18=321 - Natural grasslands
  19=322 - Moors and heathland
  2=122 - Road and rail networks and associated land
  20=323 - Sclerophyllous vegetation
  21=324 - Transitional woodland-shrub
  22=331 - Beaches, dunes, sands
  23=332 - Bare rocks
  24=333 - Sparsely vegetated areas
  25=334 - Burnt areas
  26=335 - Glaciers and perpetual snow
  27=411 - Inland wetlands
  28=421 - Maritime wetlands
  29=511 - Water courses
  3=123 - Port areas
  30=512 - Water bodies
  31=521 - Coastal lagoons
  32=522 - Estuaries
  33=523 - Sea and ocean
  4=124 - Airports
  5=131 - Mineral extraction sites
  6=132 - Dump sites
  7=133 - Construction sites
  8=141 - Green urban areas
  9=211 - Non-irrigated arable land
  AREA_OR_POINT=Area
Image Structure Metadata:
  COMPRESSION=DEFLATE
  INTERLEAVE=BAND
Corner Coordinates:
Upper Left  (  900000.000, 5460010.000) ( 55d57'42.48"W, 56d18' 4.91"N)
Lower Left  (  900000.000,  930010.000) ( 23d55'13.07"W, 24d32' 8.64"N)
Upper Right ( 6540000.000, 5460010.000) ( 61d 5'51.71"E, 64d25' 1.96"N)
Lower Right ( 6540000.000,  930010.000) ( 32d31' 8.79"E, 28d26'13.69"N)
Center      ( 3720000.000, 3195010.000) (  1d18'47.59"E, 51d32'35.92"N)
Band 1 Block=1024x1024 Type=Byte, ColorInterp=Gray
  NoData Value=0
  Overviews: 94000x75500, 47000x37750, 23500x18875, 11750x9438, 5875x4719, 2938x2360, 1469x1180, 735x590
```

Harmonized OSM Product (2021) 
-------------

This product comprises roads, railways, buildings, land use, administrative boundaries and protected nature areas from OpenStreetMaps, integrated/harmonized with [Copernicus high resolution layers](https://land.copernicus.eu/pan-european/high-resolution-layers), [NATURA2000 sites](https://www.eea.europa.eu/data-and-maps/data/natura-11) and [NUTS](https://ec.europa.eu/eurostat/web/nuts/background) (Nomenclature of territorial units for statistics). All the layers are publicly available on [Open Data Science Europe](https://maps.opendatascience.eu) and through our S3 cloud object service as [COGs](https://www.cogeo.org/) (Cloud-Optimized GeoTIFFs).

Example of how access our S3 cloud object service using ``gdalinfo`` ([complete layer list](https://gitlab.com/geoharmonizer_inea/eumap/-/blob/master/gh_raster_layers.csv)):

```bash
gdalinfo /vsicurl/http://s3.eu-central-1.wasabisys.com/eumap/adm/adm_county_nuts.osm_c_30m_0..0cm_2021_eumap_epsg3035_v0.1.tif
```

Expected output:

```
Driver: GTiff/GeoTIFF
Files: /vsicurl/http://s3.eu-central-1.wasabisys.com/eumap/adm/adm_county_nuts.osm_c_30m_0..0cm_2021_eumap_epsg3035_v0.1.tif
Size is 188000, 151000
Coordinate System is:
PROJCRS["ETRS89-extended / LAEA Europe",
    BASEGEOGCRS["ETRS89",
        DATUM["European Terrestrial Reference System 1989",
            ELLIPSOID["GRS 1980",6378137,298.257222101,
                LENGTHUNIT["metre",1]]],
        PRIMEM["Greenwich",0,
            ANGLEUNIT["degree",0.0174532925199433]],
        ID["EPSG",4258]],
    CONVERSION["Europe Equal Area 2001",
        METHOD["Lambert Azimuthal Equal Area",
            ID["EPSG",9820]],
        PARAMETER["Latitude of natural origin",52,
            ANGLEUNIT["degree",0.0174532925199433],
            ID["EPSG",8801]],
        PARAMETER["Longitude of natural origin",10,
            ANGLEUNIT["degree",0.0174532925199433],
            ID["EPSG",8802]],
        PARAMETER["False easting",4321000,
            LENGTHUNIT["metre",1],
            ID["EPSG",8806]],
        PARAMETER["False northing",3210000,
            LENGTHUNIT["metre",1],
            ID["EPSG",8807]]],
    CS[Cartesian,2],
        AXIS["northing (Y)",north,
            ORDER[1],
            LENGTHUNIT["metre",1]],
        AXIS["easting (X)",east,
            ORDER[2],
            LENGTHUNIT["metre",1]],
    USAGE[
        SCOPE["unknown"],
        AREA["Europe - LCC & LAEA"],
        BBOX[24.6,-35.58,84.17,44.83]],
    ID["EPSG",3035]]
Data axis to CRS axis mapping: 2,1
Origin = (900000.000000000000000,5460010.000000000000000)
Pixel Size = (30.000000000000000,-30.000000000000000)
Metadata:
  AREA_OR_POINT=Area
Image Structure Metadata:
  COMPRESSION=DEFLATE
  INTERLEAVE=BAND
Corner Coordinates:
Upper Left  (  900000.000, 5460010.000) ( 55d57'42.48"W, 56d18' 4.91"N)
Lower Left  (  900000.000,  930010.000) ( 23d55'13.07"W, 24d32' 8.64"N)
Upper Right ( 6540000.000, 5460010.000) ( 61d 5'51.71"E, 64d25' 1.96"N)
Lower Right ( 6540000.000,  930010.000) ( 32d31' 8.79"E, 28d26'13.69"N)
Center      ( 3720000.000, 3195010.000) (  1d18'47.59"E, 51d32'35.92"N)
Band 1 Block=1024x1024 Type=UInt16, ColorInterp=Gray
  NoData Value=0
  Overviews: 94000x75500, 47000x37750, 23500x18875, 11750x9438, 5875x4719, 2938x2360, 1469x1180, 735x590
```

List of layers available
------------------------
Available layers for continental Europe at 30m:

- Land mask,
- Digital Terrain Model (slope and elevation),
- Annual dominant land cover class based on CORINE (2000–2019),
- Annual probabilities and uncertainties for 33 CORINE land cover classes (2000–2019),
- Annual Landsat RGB composites for the summer season (June–September for 2000–2019),
- Annual Landsat NDVI composite derived by season (winter, spring, summer and fall - 2000–2020),
- Density of building areas according to OSM (2021) and Copernicus (impervious built-up - 2018),
- Density of commercial, industrial and residential buildings according to OSM (2021),
- Harmonized protected areas derived from OSM (2021) and NATURA2000 (2019),
- Harmonized administrative areas (county-level) derived from OSM (2021) and NUTS (2021),
- Probabilities of 18 OSM land cover classes (2021),
- Density of highways and railways according to OSM (2021).

**More information in [Open Data Science Europe Metadata Catalog.](https://data.opendatascience.eu/geonetwork/srv/eng/catalog.search#/home)**