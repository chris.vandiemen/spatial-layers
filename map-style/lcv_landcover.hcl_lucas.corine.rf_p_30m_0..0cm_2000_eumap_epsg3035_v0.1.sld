<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" version="1.0.0" xmlns:ogc="http://www.opengis.net/ogc" xmlns:sld="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml">
  <UserLayer>
    <sld:LayerFeatureConstraints>
      <sld:FeatureTypeConstraint/>
    </sld:LayerFeatureConstraints>
    <sld:UserStyle>
      <sld:Name>lcv_landcover.hcl_lucas.corine.rf_p_30m_0..0cm_2018_eumap_epsg3035_v0.1</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ChannelSelection>
              <sld:GrayChannel>
                <sld:SourceChannelName>1</sld:SourceChannelName>
              </sld:GrayChannel>
            </sld:ChannelSelection>
            <sld:ColorMap type="values">
              <sld:ColorMapEntry color="#ff0000" label="111-Urban fabric" quantity="1"/>
              <sld:ColorMapEntry color="#cc0000" label="122-Road and rail networks and associated land" quantity="2"/>
              <sld:ColorMapEntry color="#e6cccc" label="123-Port areas" quantity="3"/>
              <sld:ColorMapEntry color="#e6cce6" label="124-Airports" quantity="4"/>
              <sld:ColorMapEntry color="#a600cc" label="131-Mineral extraction sites" quantity="5"/>
              <sld:ColorMapEntry color="#a64d00" label="132-Dump sites" quantity="6"/>
              <sld:ColorMapEntry color="#ff4dff" label="133-Construction sites" quantity="7"/>
              <sld:ColorMapEntry color="#ffa6ff" label="133-Green urban areas" quantity="8"/>
              <sld:ColorMapEntry color="#ffffa8" label="211-Non-irrigated arable land" quantity="9"/>
              <sld:ColorMapEntry color="#ffff00" label="212-Permanently irrigated arable land" quantity="10"/>
              <sld:ColorMapEntry color="#e6e600" label="213-Rice fields" quantity="11"/>
              <sld:ColorMapEntry color="#e68000" label="221-Vineyards" quantity="12"/>
              <sld:ColorMapEntry color="#f2a64d" label="222-Fruit trees and berry plantations" quantity="13"/>
              <sld:ColorMapEntry color="#e6a600" label="223-Olive groves" quantity="14"/>
              <sld:ColorMapEntry color="#e6e64d" label="231-Pastures" quantity="15"/>
              <sld:ColorMapEntry color="#80ff00" label="311-Broad-leaved forest" quantity="16"/>
              <sld:ColorMapEntry color="#00a600" label="312-Coniferous forest" quantity="17"/>
              <sld:ColorMapEntry color="#ccf24d" label="321-Natural grasslands" quantity="18"/>
              <sld:ColorMapEntry color="#a6ff80" label="322-Moors and heathland" quantity="19"/>
              <sld:ColorMapEntry color="#a6e64d" label="323-Sclerophyllous vegetation" quantity="20"/>
              <sld:ColorMapEntry color="#a6f200" label="324-Transitional woodland-shrub" quantity="21"/>
              <sld:ColorMapEntry color="#e6e6e6" label="331-Beaches, dunes, sands" quantity="22"/>
              <sld:ColorMapEntry color="#cccccc" label="332-Bare rocks" quantity="23"/>
              <sld:ColorMapEntry color="#ccffcc" label="333-Sparsely vegetated areas" quantity="24"/>
              <sld:ColorMapEntry color="#000000" label="334-Burnt areas" quantity="25"/>
              <sld:ColorMapEntry color="#a6e6cc" label="335-Glaciers and perpetual snow" quantity="26"/>
              <sld:ColorMapEntry color="#a6a6ff" label="411-Inland wetlands" quantity="27"/>
              <sld:ColorMapEntry color="#ccccff" label="421-Maritime wetlands" quantity="28"/>
              <sld:ColorMapEntry color="#00ccf2" label="511-Water courses" quantity="29"/>
              <sld:ColorMapEntry color="#80f2e6" label="512-Water bodies" quantity="30"/>
              <sld:ColorMapEntry color="#00ffa6" label="521-Coastal lagoons" quantity="31"/>
              <sld:ColorMapEntry color="#a6ffe6" label="522-Estuaries" quantity="32"/>
              <sld:ColorMapEntry color="#e6f2ff" label="523-Sea and ocean" quantity="33"/>
            </sld:ColorMap>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </UserLayer>
</StyledLayerDescriptor>
