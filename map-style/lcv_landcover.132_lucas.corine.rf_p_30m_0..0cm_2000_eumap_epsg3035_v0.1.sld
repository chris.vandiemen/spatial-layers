<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" version="1.0.0" xmlns:ogc="http://www.opengis.net/ogc" xmlns:sld="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml">
  <UserLayer>
    <sld:LayerFeatureConstraints>
      <sld:FeatureTypeConstraint/>
    </sld:LayerFeatureConstraints>
    <sld:UserStyle>
      <sld:Name>lcv_landcover.132_lucas.corine.rf_p_30m_0..0cm_2000_eumap_epsg3035_v0.1</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ChannelSelection>
              <sld:GrayChannel>
                <sld:SourceChannelName>1</sld:SourceChannelName>
              </sld:GrayChannel>
            </sld:ChannelSelection>
            <sld:ColorMap>
              <sld:ColorMapEntry color="#f3f0ef" label="10" quantity="10" opacity="1.0"/>
              <sld:ColorMapEntry color="#e0c7b3" label="20" quantity="20" opacity="1.0"/>
              <sld:ColorMapEntry color="#cc9e77" label="30" quantity="30" opacity="1.0"/>
              <sld:ColorMapEntry color="#b9753b" label="40" quantity="40" opacity="1.0"/>
              <sld:ColorMapEntry color="#a64d00" label="50" quantity="50" opacity="1.0"/>
            </sld:ColorMap>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </UserLayer>
</StyledLayerDescriptor>
