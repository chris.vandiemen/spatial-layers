<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" version="1.0.0" xmlns:ogc="http://www.opengis.net/ogc" xmlns:sld="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml">
  <UserLayer>
    <sld:LayerFeatureConstraints>
      <sld:FeatureTypeConstraint/>
    </sld:LayerFeatureConstraints>
    <sld:UserStyle>
      <sld:Name>lcv_landcover.523_lucas.corine.rf_p_30m_0..0cm_2000_eumap_epsg3035_v0.1</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ChannelSelection>
              <sld:GrayChannel>
                <sld:SourceChannelName>1</sld:SourceChannelName>
              </sld:GrayChannel>
            </sld:ChannelSelection>
            <sld:ColorMap>
              <sld:ColorMapEntry color="#eff1f3" label="10" quantity="10" opacity="1.0"/>
              <sld:ColorMapEntry color="#edf1f6" label="20" quantity="20" opacity="1.0"/>
              <sld:ColorMapEntry color="#ebf1f9" label="30" quantity="30" opacity="1.0"/>
              <sld:ColorMapEntry color="#e8f2fc" label="40" quantity="40" opacity="1.0"/>
              <sld:ColorMapEntry color="#e6f2ff" label="50" quantity="50" opacity="1.0"/>
            </sld:ColorMap>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </UserLayer>
</StyledLayerDescriptor>
